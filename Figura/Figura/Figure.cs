﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GeometryLibrary
{
    /// <summary>
    /// Базовый класс для геометрических фигур.
    /// </summary>
    public abstract class Figure
    {
        #region Поля и свойства

        /// <summary>
        /// Список точек, определяющих фигуру.
        /// </summary>
        protected List<Point> points;

        #endregion

        #region Методы

        /// <summary>
        /// Проверяет, существует ли фигура, проверяя количество точек.
        /// </summary>
        /// <returns>True, если фигура существует, в противном случае - false.</returns>
        public virtual bool Exists()
        {
            return this.points.Count >= 3;
        }

        /// <summary>
        /// Возвращает список точек, определяющих фигуру. 
        /// </summary>
        /// <returns>Список точек.</returns>
        public List<Point> GetPoints()
        {
            return this.points;
        }

        /// <summary>
        /// Вычисляет длины сторон фигуры.
        /// </summary>
        /// <returns>Список длин сторон.</returns>
        public virtual List<double> GetSideLengths()
        {
            var sideLengths = new List<double>();
            for (int i = 0; i < this.points.Count; i++)
            {
                var nextIndex = (i + 1) % this.points.Count;
                sideLengths.Add(this.points[i].DistanceTo(this.points[nextIndex]));
            }
            return sideLengths;
        }

        /// <summary>
        /// Вычисляет периметр фигуры.
        /// </summary>
        /// <returns>Значение периметра.</returns>
        public virtual double GetPerimeter()
        {
            var sideLengths = GetSideLengths();
            return sideLengths.Sum();
        }

        /// <summary>
        /// Вычисляет площадь фигуры.
        /// </summary>
        /// <returns>Значение площади.</returns>
        public abstract double GetArea();

        #endregion
    

        #region Конструкторы

        /// <summary>
        /// Создает новый экземпляр класса Figure с заданными точками.
        /// </summary>
        /// <param name="points">Список точек, определяющих фигуру.</param>
        public Figure(List<Point> points)
        {
            this.points = points;
        }

        #endregion

    }
}
