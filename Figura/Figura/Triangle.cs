﻿using System;
using System.Collections.Generic;

namespace GeometryLibrary
{
    /// <summary>
    /// Представляет треугольник, определенный тремя точками.
    /// </summary>
    public class Triangle : Figure
    {
        #region Методы

        /// <summary>
        /// Проверяет, существует ли треугольник путем проверки количества точек.
        /// </summary>
        /// <returns>True, если треугольник существует, в противном случае - false.</returns>
        public override bool Exists()
        {
            return base.points.Count == 3;
        }

        /// <summary>
        /// Вычисляет угол при указанной вершине треугольника.
        /// </summary>
        /// <param name="vertexIndex">Индекс вершины треугольника.</param>
        /// <returns>Значение угла в градусах.</returns>
        public double GetAngle(int vertexIndex)
        {
            if (vertexIndex < 0 || vertexIndex >= base.points.Count)
                throw new ArgumentOutOfRangeException("Invalid vertex index.");

            var prevIndex = (vertexIndex + base.points.Count - 1) % base.points.Count;
            var nextIndex = (vertexIndex + 1) % base.points.Count;

            var prevPoint = base.points[prevIndex];
            var vertex = base.points[vertexIndex];
            var nextPoint = base.points[nextIndex];

            var a = prevPoint.DistanceTo(vertex);
            var b = vertex.DistanceTo(nextPoint);
            var c = prevPoint.DistanceTo(nextPoint);

            return Math.Acos((a * a + b * b - c * c) / (2 * a * b)) * (180 / Math.PI);
        }

        /// <summary>
        /// Вычисляет площадь треугольника.
        /// </summary>
        /// <returns>Значение площади.</returns>
        public override double GetArea()
        {
            var a = base.points[0].DistanceTo(base.points[1]);
            var b = base.points[1].DistanceTo(base.points[2]);
            var c = base.points[2].DistanceTo(base.points[0]);

            var s = (a + b + c) / 2;
            return Math.Sqrt(s * (s - a) * (s - b) * (s - c));
        }

        #endregion

        #region Конструкторы

        /// <summary>
        /// Создает новый экземпляр класса Triangle с заданными точками вершин. 
        /// </summary>
        /// <param name="points">Список точек, определяющих вершины треугольника.</param>
        public Triangle(List<Point> points) : base(points) { }

        #endregion
  
    }
}
