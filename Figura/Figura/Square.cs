﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GeometryLibrary
{
    /// <summary>
    /// Представляет четырехугольник, определенный четырьмя точками.
    /// </summary>
    public class Square : Figure
    {
        #region Методы

        /// <summary>
        /// Проверяет, существует ли четырехугольник путем проверки количества точек.
        /// </summary>
        /// <returns>True, если четырехугольник существует, в противном случае - false.</returns>
        public override bool Exists()
        {
            return base.points.Count == 4;
        }

        /// <summary>
        /// Вычисляет длины сторон четырехугольника.
        /// </summary>
        /// <returns>Список длин сторон.</returns>
        public override List<double> GetSideLengths()
        {
            var sideLengths = new List<double>();
            for (int i = 0; i < base.points.Count; i++)
            {
                var nextIndex = (i + 1) % base.points.Count;
                sideLengths.Add(base.points[i].DistanceTo(base.points[nextIndex]));
            }
            return sideLengths;
        }

        /// <summary>
        /// Вычисляет углы четырехугольника.
        /// </summary>
        /// <returns>Углы четырехугольника в градусах.</returns>
        public List<double> GetAngles()
        {
            var angles = new List<double>();
            for (int i = 0; i < base.points.Count; i++)
            {
                var prevIndex = (i + base.points.Count - 1) % base.points.Count;
                var nextIndex = (i + 1) % base.points.Count;

                var prevPoint = base.points[prevIndex];
                var vertex = base.points[i];
                var nextPoint = base.points[nextIndex];

                var angle = CalculateAngle(prevPoint, vertex, nextPoint);
                angles.Add(angle);
            }
            return angles;
        }

        /// <summary>
        /// Вычисляет площадь четырехугольника.
        /// </summary>
        /// <returns>Значение площади.</returns>
        public override double GetArea()
        {
            if (!this.Exists())
                return 0;

            // Вычисляем площадь четырехугольника по формуле Гаусса.
            var sideLengths = GetSideLengths();
            var s1 = sideLengths[0];
            var s2 = sideLengths[2];
            var angle = GetAngles()[1];

            var area = 0.5 * s1 * s2 * Math.Sin(angle * Math.PI / 180);
            return area;
        }

        /// <summary>
        /// Вычисляет угол между тремя точками.
        /// </summary>
        private double CalculateAngle(Point point1, Point vertex, Point point2)
        {
            var a = point1.DistanceTo(vertex);
            var b = vertex.DistanceTo(point2);
            var c = point1.DistanceTo(point2);

            var cosAlpha = (a * a + b * b - c * c) / (2 * a * b);
            var angleRad = Math.Acos(cosAlpha);

            return angleRad * (180 / Math.PI);
        }

        #endregion

        #region Конструкторы

        /// <summary>
        /// Создает новый экземпляр класса Square с заданными точками вершин. 
        /// </summary>
        /// <param name="points">Список точек, определяющих вершины четырехугольника.</param>
        public Square(List<Point> points) : base(points) { }

        #endregion

    }
}
