﻿using System;
using System.Collections.Generic;

namespace GeometryLibrary
{
    /// <summary>
    /// Представляет шестиугольник, определенный шестью точками.
    /// </summary>
    public class Hexagon : Figure
    {
        #region Методы

        /// <summary>
        /// Проверяет, существует ли шестиугольник путем проверки количества точек.
        /// </summary>
        /// <returns>True, если шестиугольник существует, в противном случае - false.</returns>
        public override bool Exists()
        {
            return points.Count == 6;
        }

        /// <summary>
        /// Вычисляет длины сторон шестиугольника.
        /// </summary>
        /// <returns>Список длин сторон.</returns>
        public override List<double> GetSideLengths()
        {
            var sideLengths = new List<double>();
            for (int i = 0; i < points.Count; i++)
            {
                var nextIndex = (i + 1) % base.points.Count;
                sideLengths.Add(points[i].DistanceTo(points[nextIndex]));
            }
            return sideLengths;
        }

        /// <summary>
        /// Вычисляет углы шестиугольника.
        /// </summary>
        /// <returns>Углы шестиугольника в градусах.</returns>
        public List<double> GetAngles()
        {
            var angles = new List<double>();
            for (int i = 0; i < points.Count; i++)
            {
                var prevIndex = (i + points.Count - 1) % points.Count;
                var nextIndex = (i + 1) % points.Count;

                var prevPoint = points[prevIndex];
                var vertex = points[i];
                var nextPoint = points[nextIndex];

                // Разбиваем шестиугольник на четыре треугольника и вычисляем углы каждого треугольника.
                var trianglePoints = new List<Point> { prevPoint, vertex, nextPoint };
                var triangle = new Triangle(trianglePoints);
                angles.Add(triangle.GetAngle(1)); // Угол при вершине vertex (с индексом 1) в каждом треугольнике.
            }
            return angles;
        }

        /// <summary>
        /// Вычисляет площадь шестиугольника.
        /// </summary>
        /// <returns>Значение площади.</returns>
        public override double GetArea()
        {
            if (!this.Exists())
                return 0;

            // Предполагаем, что шестиугольник правильный и все стороны равны.
            var sideLength = base.points[0].DistanceTo(base.points[1]);

            var area = (3 * Math.Sqrt(3) * sideLength * sideLength) / 2;
            return area;
        }

        #endregion

        #region Конструкторы

        /// <summary>
        /// Создает новый экземпляр класса Hexagon с заданными точками вершин. 
        /// </summary>
        /// <param name="points">Список точек, определяющих вершины шестиугольника.</param>
        public Hexagon(List<Point> points) : base(points) { }

        #endregion
   
    }
}
