﻿using System;

/// <summary>
/// Представляет точку в 2D пространстве с координатами X и Y. 
/// </summary>
public class Point
{
    #region Поля и свойства

    /// <summary>
    /// Получает координату X точки.
    /// </summary>
    public double X { get; }

    /// <summary>
    /// Получает координату Y точки.
    /// </summary>
    public double Y { get; }

    #endregion

    #region Методы

    /// <summary>
    /// Вычисляет расстояние до другой точки.
    /// </summary>
    /// <param name="other">Другая точка.</param>
    /// <returns>Расстояние до другой точки.</returns>
    public double DistanceTo(Point other)
    {
        var dx = this.X - other.X;
        var dy = this.Y - other.Y;
        return Math.Sqrt(dx * dx + dy * dy);
    }

    #endregion

    #region Конструкторы

    /// <summary>
    /// Инициализирует новый экземпляр класса Point с указанными координатами X и Y.
    /// </summary>
    /// <param name="x">Координата X.</param>
    /// <param name="y">Координата Y.</param>
    public Point(double x, double y)
    {
        this.X = x;
        this.Y = y;
    }

    #endregion
}
