﻿using System;
using System.Collections.Generic;

namespace GeometryLibrary
{
    public class Program
    {
        public static void Main()
        {
            while (true)
            {
                Console.WriteLine("Выберите фигуру для анализа: ");
                Console.WriteLine("1. Треугольник");
                Console.WriteLine("2. Квадрат");
                Console.WriteLine("3. Шестиугольник");
                Console.WriteLine("4. Выход");

                var input = Console.ReadLine();

                switch (input)
                {
                    case "1":
                        AnalyzeTriangle();
                        break;
                    case "2":
                        AnalyzeSquare();
                        break;
                    case "3":
                        AnalyzeHexagon();
                        break;
                    case "4":
                        return; // Выход из программы
                    default:
                        Console.WriteLine("Неверный выбор. Пожалуйста, введите число от 1 до 4.");
                        break;
                }

                Console.WriteLine();
            }
        }

        /// <summary>
        /// Анализирует треугольник.
        /// </summary>
        private static void AnalyzeTriangle()
        {
            var trianglePoints = ReadPointsFromUser(3);
            var triangle = new Triangle(trianglePoints);
            PrintFigureInfo(triangle);
        }

        /// <summary>
        /// Анализирует квадрат.
        /// </summary>
        private static void AnalyzeSquare()
        {
            var squarePoints = ReadPointsFromUser(4);
            var square = new Square(squarePoints);
            PrintFigureInfo(square);
        }

        /// <summary>
        /// Анализирует шестиугольник.
        /// </summary>
        private static void AnalyzeHexagon()
        {
            var hexagonPoints = ReadPointsFromUser(6);
            var hexagon = new Hexagon(hexagonPoints);
            PrintFigureInfo(hexagon);
        }

        /// <summary>
        /// Считывает координаты точек от пользователя.
        /// </summary>
        /// <param name="numberOfPoints">Количество точек для считывания.</param> 
        /// <returns>Список точек.</returns>
        private static List<Point> ReadPointsFromUser(int numberOfPoints)
        {
            var points = new List<Point>();
            for (int i = 0; i < numberOfPoints; i++)
            {
                Console.Write($"Точка {i + 1}: ");
                var input = Console.ReadLine();
                var coordinates = input.Split(' ');
                if (coordinates.Length != 2 || !double.TryParse(coordinates[0], out double x) || !double.TryParse(coordinates[1], out double y))
                {
                    Console.WriteLine("Неверный формат ввода. Введите координаты в формате 'x y'.");
                    i--;
                    continue;
                }
                points.Add(new Point(x, y));
            }
            return points;
        }

        /// <summary>
        /// Выводит информацию о фигуре, такую как ее существование, длины сторон, периметр, углы и площадь.
        /// </summary>
        /// <param name="figure">Объект фигуры.</param>
        private static void PrintFigureInfo(Figure figure)
        {
            var points = figure.GetPoints();
            Console.WriteLine($"Фигура существует: {figure.Exists()}");
            var sideLengths = figure.GetSideLengths();
            Console.WriteLine("Длины сторон: " + string.Join(", ", sideLengths));
            Console.WriteLine($"Периметр: {figure.GetPerimeter()}");

            if (figure is Triangle triangle)
            {
                for (int i = 0; i < points.Count; i++)
                {
                    Console.WriteLine($"Угол при вершине {i + 1}: {triangle.GetAngle(i)} градусов");
                }
            }
            else if (figure is Square square)
            {
                Console.WriteLine($"Углы: {string.Join(", ", square.GetAngles())} градусов");
            }
            else if (figure is Hexagon hexagon)
            {
                Console.WriteLine($"Углы: {string.Join(", ", hexagon.GetAngles())} градусов");
            }

            // Вычисляем и выводим площадь фигуры
            var area = figure.GetArea();
            Console.WriteLine($"Площадь: {area}");
            Console.WriteLine();
        }
    }
}
